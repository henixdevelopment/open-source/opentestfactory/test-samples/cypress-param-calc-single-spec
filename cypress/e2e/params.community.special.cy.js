import { checkCommonParams, checkUltimateParams } from "../support/paramChecks";

describe('Special Characters Parameters Community Test', () => {

  beforeEach(function() {
    // Load fixture data before each test
    cy.fixture('params').then((params) => {
      this.params = params;
    });
  });

  it('Special Characters Parameters Check Community', function() {
    checkCommonParams(this.params);
  });
});