import { checkCommonParams, checkUltimateParams } from "../support/paramChecks";

describe('Special Characters Parameters Ultimate Test', () => {

  beforeEach(function() {
    // Load fixture data before each test
    cy.fixture('params').then((params) => {
      this.params = params;
    });
  });

  it('Special Characters Parameters Check Ultimate', function() {
    checkCommonParams(this.params);
    checkUltimateParams(this.params);
  });
});