// cypress/support/paramChecks.js
export const checkCommonParams = (params) => {
  expect(Cypress.env('DSNAME')).to.equal(params.DSNAME);
  expect(Cypress.env('DS_FIRST_PARAM')).to.equal(params.DS_FIRST_PARAM);
  expect(Cypress.env('TC_CUF_cy_testcase')).to.equal(params.TC_CUF_cy_testcase);
};

export const checkUltimateParams = (params) => {
  checkCommonParams(params);
  expect(Cypress.env('IT_CUF_cy_iteration')).to.equal(params.IT_CUF_cy_iteration);
  expect(Cypress.env('CPG_CUF_cy_campaign')).to.equal(params.CPG_CUF_cy_campaign);
  expect(Cypress.env('TS_CUF_cy_testsuite')).to.equal(params.TS_CUF_cy_testsuite);
};
