# Project Description and Setup Instructions

This project involves testing Community and Ultimate parameters. The Community parameters are common for Community and Ultimate licences.

## Steps to Run the Tests

### When Using Squash TM:
1. **For Community testing:**
   - Set dataset `DSNAME` to: `dsn[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
   - Set parameter `DS_FIRST_PARAM` to: `ds[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
   - Set test case CUF `cy_testcase` to: `tc[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`

2. **For Ultimate testing:**
   - Set test case CUF `cy_iteration` to: `it[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
   - Set test case CUF `cy_campaign` to: `cpg[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
   - Set test case CUF `cy_testsuite` to: `ts[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`

### Without Squash TM:
Set the following environment variables: 
1. **DSNAME:** `dsn[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
2. **DS_FIRST_PARAM:** `ds[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
3. **TC_CUF_cy_testcase:** `tc[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
4. **IT_CUF_cy_iteration:** `it[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
5. **CPG_CUF_cy_campaign:** `cpg[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
6. **TS_CUF_cy_testsuite:** `ts[0] ^ %VAR% #<9:/&é"(-è_çà)=he | \pe(pe); $var`
